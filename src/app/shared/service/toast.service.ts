import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable()
export class ToastService {

    constructor(public toastController: ToastController) {}

    async presentToast(msg) {
        const toast = await this.toastController.create({
            message: msg,
            duration: 4000
        });
        toast.present();
    }

    async presentToastError(msg) {
        const toast = await this.toastController.create({
            message: msg,
            cssClass: 'toast-error',
            duration: 5000,
            showCloseButton: true,
            position: 'top',
            closeButtonText: 'Done'
        });
        toast.present();
    }
}
