import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Injectable()
export class AlertService {

    constructor(public alertCtrl: AlertController) {}

    async presentAlert(msg) {
        const alert = await this.alertCtrl.create({
            header: 'Alert',
            // subHeader: 'Subtitle',
            message: msg,
            buttons: ['OK']
        });

        await alert.present();
    }
}
