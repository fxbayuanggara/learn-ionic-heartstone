import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Firebase } from '@ionic-native/firebase/ngx';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable()
export class FcmService {
    constructor(private platform: Platform, private firebase: Firebase, private afs: AngularFirestore) {}

    async getToken() {
        let token;

        if (this.platform.is('android')) {
            token = await this.firebase.getToken();
        }

        if (this.platform.is('ios')) {
            token = await this.firebase.getToken();
            this.firebase.grantPermission();
        }

        this.saveToken(token);
    }

    private saveToken(token) {
        if (!token) return;

        const deviceRef = this.afs.collection('devices');

        const data = {
            token,
            userId: 'testUser'
        };

        return deviceRef.doc(token).set(data);
    }

    onNotification() {
        return this.firebase.onNotificationOpen();
    }
}
