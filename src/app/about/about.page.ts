import { Component } from '@angular/core';
import { ToastService } from '../shared/service/toast.service';

@Component({
  selector: 'app-about',
  templateUrl: 'about.page.html',
  styleUrls: ['about.page.scss']
})
export class AboutPage {
  constructor(private toaster: ToastService) {}

  ionViewDidEnter() {
    this.toaster.presentToast('About page openned on lifecycle');
  }
}
