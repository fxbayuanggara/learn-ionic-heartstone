import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { CardService } from '../shared/card.service';
import { LoaderService } from '../../shared/service/loader.service';
import { Storage } from '@ionic/storage';
import { FavoriteCardStore } from '../shared/card-favorite.store';

import { Card } from '../shared/card.model';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-card-listing',
  templateUrl: './card-listing.page.html',
  styleUrls: ['./card-listing.page.scss'],
})
export class CardListingPage {

  cardDeckGroup: string;
  cardDeck: string;
  isLoading = false;
  favoriteCards: any = {};
  favoriteCardSub: Subscription;

  cards: Card[] = [];
  copyOfCards: Card[] = [];

  limit = 20;

  constructor(private route: ActivatedRoute,
              private cardService: CardService,
              private loaderService: LoaderService,
              private storage: Storage,
              private favoriteCardStore: FavoriteCardStore) {

      this.favoriteCardSub = this.favoriteCardStore.favoriteCards.subscribe((favoriteCards) => {
          this.favoriteCards = favoriteCards;
      });
  }

  ionViewWillLeave() {
      if (this.favoriteCardSub && !this.favoriteCardSub.closed) {
          this.favoriteCardSub.unsubscribe();
      }
  }

  async getCards() {
      await this.loaderService.presentLoading();

      this.cardService.getCardsByDeck(this.cardDeckGroup, this.cardDeck).subscribe(
          (cards: Card[]) => {
              this.cards = cards.map((card: Card) => {
                  card.text = this.cardService.replaceCardTextLine(card.text);
                  card.favorite = this.isCardFavorite(card.cardId);
                  return card;
              });

              this.copyOfCards = Array.from(this.cards);
              this.loaderService.dismissLoading();
          });
  }

  private isCardFavorite(cardId: string): boolean {
      const card = this.favoriteCards[cardId];
      return card ? true : false;
  }

  async ionViewWillEnter() {
    this.cardDeckGroup = this.route.snapshot.paramMap.get('cardDeckGroup');
    this.cardDeck = this.route.snapshot.paramMap.get('cardDeck');

    if (this.cards && this.cards.length === 0) { this.getCards(); }
  }

    // ngOnDestroy() {
    //     alert('destroying component card listing');
    // }

    doRefresh(event) {
        this.getCards();
        event.target.complete();
    }

    hydrateCards(card: Card[]) {
        this.cards = card;
        this.isLoading = false;
    }

    handleStartedSearch() {
        this.isLoading = true;
    }

    favoriteCard(card: Card) {
        this.favoriteCardStore.toggleCards(card);
    }

    loadData(infiniteScroll) {
      setTimeout(() => {
          this.limit += 20;
          infiniteScroll.target.complete();
      }, 400);
    }

}
